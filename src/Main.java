import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Main {

    // Complete the rotLeft function below.
    static int[] rotLeft(int[] a, int d) {
        int tmp[] = new int[1];

        for(int i=0;i<d;i++){
            tmp[0]=a[0];
            for (int j=0;j<a.length-1;j++){
                a[j]=a[j+1];
            }
            a[a.length-1] = tmp[0];
        }

        return a;

    }


    public static void main(String[] args) throws IOException {

        int d = 4;
        int[] a = {1,2,3,4,5};
        int[] result = rotLeft(a, d);

        for (int i = 0; i < result.length; i++) {
           System.out.println(result[i]);

        }

    }
}
